#include <cstdlib>
#include <iostream>

#include "Game.h"

#define LEN_X 10
#define LEFT 'a'
#define RIGHT 'd'

using std::cin;
using std::cout;
using std::endl;

void print_game_state(unsigned int player_pos, unsigned goal,
                      unsigned int start) {
  char game_state[LEN_X];

  for (int i = 0; i < LEN_X; i++) {
    game_state[i] = '.';
  }

  game_state[start] = '|';
  game_state[goal] = '|';
  game_state[player_pos] = 'P';

  for (int i = 0; i < LEN_X; i++) {
    cout << game_state[i]<<" ";
  }
  cout << endl;
}

unsigned int execute_move(unsigned int player_pos, char move) {
  switch (move) {
  case LEFT:
    if (player_pos > 0) {

      player_pos--;
      cout << "you moved left!" << endl;
    } else {
      cout << "you bounced" << endl;
    }
    break;
  case RIGHT:
    if (player_pos < LEN_X - 2) {

      player_pos++;
      cout << "you moved right!" << endl;
    } else {
      cout << "you won" << endl;
      return 0;
    }
    break;
  default:
    break;
  }
  return player_pos;
}

bool is_finished(unsigned int player_pos, unsigned int goal) {
  return player_pos == goal;
}

void game() {
  unsigned int player_pos = 0;
  unsigned int start = 0;
  unsigned int goal = LEN_X - 1;

  char move;
  bool finished = false;
  do {
    print_game_state(player_pos, goal, start);
    cin >> move;
    system("CLS");

    player_pos = execute_move(player_pos, move);

    finished = is_finished(player_pos, goal);

  } while (!finished);
}