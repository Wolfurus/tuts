#pragma once

#include "DynArray.h"

/// <summary>
/// 
/// </summary>
/// <param name="dynamic_array"></param>
/// <returns></returns>
double sum(DynamicArray &dynamic_array);

/// <summary>
/// 
/// </summary>
/// <param name="dynamic_array"></param>
/// <returns></returns>
double mean(DynamicArray &dynamic_array);

/// <summary>
///	
/// </summary>
/// <param name="dynamic_array"></param>
/// <returns></returns>
double median(DynamicArray &dynamic_array);

/// <summary>
/// 
/// </summary>
/// <param name="dynamic_array"></param>
/// <returns></returns>
double variance(DynamicArray &dynamic_array);

/// <summary>
///
/// </summary>
/// <param name="dynamic_array"></param>
/// <returns></returns>
double stddev(DynamicArray &dynamic_array);