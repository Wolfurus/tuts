#pragma once

#include <cstddef>

/// <summary>
/// DynamicArray Structure.
/// <param name="m_data">Data</param>
/// <param name="m_length">Length</param>
/// </summary>
struct DynamicArray {
  double *m_data;
  std::size_t m_length;
};

/// <summary>
/// Creat a dynamic array object.
/// </summary>
/// <param name="value">The fill value for the array</param>
/// <param name="length">The length of the array.</param>
/// <returns>DynamicArray</returns>
DynamicArray create_dynamic_array(const double &value,
                                  const std::size_t &length);

/// <summary>
/// Push backs the *value* at the end of the array.
/// </summary>
/// <param name="dynamic_array">The dynamic array.</param>
/// <param name="value">The value to append to the array.</param>
void push_back(DynamicArray & dynamic_array, const int &value);

/// <summary>
/// Pop backs the value at the end of the vector.
/// </summary>
/// <param name="dynamic_array">The dynamic array.</param>
/// <param name="value">The value to append to the array.</param>
void pop_back(DynamicArray &dynamic_array);
