#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "exercise.h"

using std::cin;
using std::cout;
using std::endl;
using std::map;
using std::pair;
using std::string;
using std::tuple;
using std::vector;

// In eurem Programm bitte:
// your_main zu main �ndern
int main() {
  // int your_main() {
  // Aufgabe 1
  Friends friends;
  friends["Dietmar"] = {20, 20};
  friends["Jennifer"] = {30, 10};
  // Aufgabe 2
  std::cout << get_oldest_friend(friends) << std::endl;
  std::cout << get_heaviest_friend(friends) << std::endl;

  return 0;
}