#include <algorithm>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

#include "exercise.h"

using std::cin;
using std::cout;
using std::endl;
using std::map;
using std::pair;
using std::string;
using std::tuple;
using std::vector;

// Aufgabe 2

bool sort_oldest(const pair<int, int> &a, const pair<int, int> &b) {
  return a.first > b.first;
}

bool sort_heaviest(const pair<int, int>& a, const pair<int, int>& b) {
  return a.second > b.second;
}

string get_oldest_friend(const Friends &friends) {
  string oldest_friend = "";
  int oldest_weight = 0;

  for (const auto &val : friends) {
    std::pair<int, int> p = val.second;
    if (p.first > oldest_weight) {
      oldest_weight = p.first;
      oldest_friend = val.first;
    }
  }
  return oldest_friend;
}

string get_heaviest_friend(const Friends &friends) {
  string heaviest_friend = "";
  int heaviest_weight = 0;

  for (const auto &val : friends) {
    std::pair<int, int> p = val.second;
    if (p.second > heaviest_weight) {
      heaviest_weight = p.second;
      heaviest_friend = val.first;
    }
  }
  return heaviest_friend;
}
