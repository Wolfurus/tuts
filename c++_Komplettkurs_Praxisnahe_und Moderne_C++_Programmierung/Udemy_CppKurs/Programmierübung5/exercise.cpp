#include <algorithm>
#include <iostream>
#include <vector>

#include "exercise.h"

using std::cin;
using std::cout;
using std::endl;
using std::fill;
using std::sort;
using std::vector;

bool my_sorting_ascending(const double &i, const double &j) { return i < j; }

// Aufgabe 2
vector<double> max_row_values(Matrix &matrix) {
  std::vector<double> temp(matrix.size());
  for (size_t i = 0; i < matrix.size(); i++) {
    for (size_t j = 0; j < matrix[i].size(); j++) {
      if (temp[i]<matrix[i][j]) {
        temp[i] = matrix[i][j];
      }
    }
  }
  return temp;
}

// Aufgabe 3
double sort_and_max(vector<double> &vec) {
  std::sort(vec.begin(), vec.end(), my_sorting_ascending);
  return vec[vec.size() - 1];
}

