#include "exercise.h"
#include <iostream>
#include <string>

using std::cin;
using std::cout;
using std::endl;
using std::string;

// Aufgabe 1
uchar hex_xor(uchar &hex1, uchar &hex2) { return (hex1 | hex2); }

// Aufgabe 2
ByteArray hex_vector_xor(ByteArray &vec1, ByteArray &vec2) {

  ByteArray temp;
  size_t length = vec1.size() > vec2.size() ? vec1.size() : vec2.size();

  for (size_t i = 0; i < length; i++) {
    temp[i] = hex_xor(vec1[i], vec2[i]);
  }
  return temp;
}