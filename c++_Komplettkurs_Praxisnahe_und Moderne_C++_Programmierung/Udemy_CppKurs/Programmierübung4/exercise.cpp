#include "exercise.h"
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

namespace computation {
// Aufgabe 1

double mean_array_value(int *array, const unsigned int &length) {
  double avg = 0;
  for (int i = 0; i < length; i++) {
    avg += array[i];
  }
  avg = avg / length;
  return avg;
}

double mean_array_value(double *array, const unsigned int &length) {
  double avg = 0;
  for (int i = 0; i < length; i++) {
    avg += array[i];
  }
  avg = avg / length;
  return avg;
}

} // namespace computation