#include <iostream>

#include "exercise.h"

using std::cout;
using std::endl;

// In eurem Programm bitte:
// your_main zu main �ndern
int main() {
  // int your_main() {
  // Aufgabe 1
  double *double_array = new double[100];
  for (int i = 0; i < 100; i++) {
    double_array[i] = i;
  }
  double sum = array_sum(double_array, 100);
  cout << sum << endl;

  // Aufgabe 2

  int *int_array = array_constructor(5, 3);

  for (int i = 0; i < 3; i++) {
    cout << int_array[i] << endl;
  }

  return 0;
}