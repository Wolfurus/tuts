#include "Exercise.h"

bool is_palindrom(const std::string &str) {
  auto itf = str.begin();
  auto itb = str.rbegin();
  while (itf != str.end() && itb != str.rend()) {
    if (*itf != *itb) {
      return false;
    }
    itf++;
    itb++;
  }
  return true;
}

bool is_in_string(const std::string str, const std::string sub_str) {
  int temp = 0;
  for (int i = 0; i < str.length(); i++) {
    for (int j = 0; j < sub_str.length(); j++) {
      if (j == 0) {
        temp = i;
      }
      if (str[i] != sub_str[j] && i != str.length() - 1) {
        j = sub_str.length();
        i = temp;
      } else if (j == sub_str.length() - 1) {
        return true;
      }
    }
  }
  return false;
}

unsigned int str_concat_exponent(const std::string str,
                                 const std::string base) {
  int counter = str.length()/base.length();
  std::regex regexp("(" + base + "){" + std::to_string(counter) + "}");
  if (!regex_match(str, regexp))
    return 0;
  return counter;
}
