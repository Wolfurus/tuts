#include <algorithm>
#include <iostream>
#include <regex>
#include <string>

// Aufgabe 1
bool is_palindrom(const std::string &str);

// Aufgabe 2
bool is_in_string(const std::string str, const std::string sub_str);

// Aufgabe 3
unsigned int str_concat_exponent(const std::string str, const std::string base);