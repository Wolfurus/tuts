#include <iostream>

#include "exercise.h"

// Aufgabe 1
void push_back(int *&input_array, const unsigned int &size, const int &value) {
  int *int_array = new int[size+1];
  for (int i = 0; i < size; i++) {
    int_array[i] = input_array[i];
  }
  int_array[size] = value;
  delete[] input_array;
  input_array = int_array;
}

// Aufgabe 2
void pop_back(int *&input_array, const unsigned int &size) {
  int *int_array = new int[size - 1];
  for (int i = 0; i < size-1; i++) {
    int_array[i] = input_array[i];
  }
  delete[] input_array;
  input_array = int_array;
}