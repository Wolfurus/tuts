//#include <iostream>
//
//using std::cout;
//using std::endl;
//
//// Enum: Enumeration
//enum PermissionLevel { STUDENT = 1, TUTOR = 2, DOZENT = 3, ADMIN = 4 };
//
//struct User {
//  int id;
//  PermissionLevel permission;
//};
//
//int main() {
//  User jan{10240240240, PermissionLevel::STUDENT};
//
//
//  switch (jan.permission) {
//  case PermissionLevel::STUDENT:
//    cout << "Ich bin ein Student!" << endl;
//    break;
//  case PermissionLevel::TUTOR:
//    cout << "Ich bin ein Tutor!" << endl;
//    break;
//  case PermissionLevel::DOZENT:
//    cout << "Ich bin ein Dozent!" << endl;
//    break;
//  case PermissionLevel::ADMIN:
//    cout << "Ich bin ein Admin!" << endl;
//    break;
//  default:
//    break;
//  }
//  return 0;
//}