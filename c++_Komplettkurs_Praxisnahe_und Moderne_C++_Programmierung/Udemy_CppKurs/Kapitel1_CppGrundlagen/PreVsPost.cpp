//#include <iostream>
//
//using std::cout;
//using std::endl;
//
//int main() {
//  int val = 0;
//
//  // Post: Erst die Code Zeile ausfueren, dann den Wert erhoehen
//  cout << val++ << endl;
//  // Pre: Erst den Wert erhoehen, dann den Rest der Code Yeile ausfuehren
//  cout << ++val << endl;
//}