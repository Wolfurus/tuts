//#include <iostream>
//
//using std::cin;
//using std::cout;
//using std::endl;
//
//int main() {
//  // sizeof()
//
//  char a = 1000000;
//  short b = 20000000;
//  int c = 3000000000;
//  long long int d = 400000000;
//  float e = 5.0000000000;
//  double f = 6.0000000000;
//
//  cout << "Char memory size in Bytes: " << sizeof(a) << endl;
//  cout << "Short memory size in Bytes: " << sizeof(b) << endl;
//  cout << "Integer memory size in Bytes: " << sizeof(c) << endl;
//  cout << "Long memory size in Bytes: " << sizeof(d) << endl;
//  cout << "Float memory size in Bytes: " << sizeof(e) << endl;
//  cout << "Double memory size in Bytes: " << sizeof(f) << endl;
//}