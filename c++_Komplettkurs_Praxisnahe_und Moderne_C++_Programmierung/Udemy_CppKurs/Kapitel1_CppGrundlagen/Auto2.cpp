//#include <algorithm>
//#include <iostream>
//#include <numeric>
//#include <vector>
//
//int main() {
//  // auto val : vec (COPY BY VALUE) change local copie
//  // auto &val : vec (CALL BY REFERENCE) change vector
//  // const auto &val : vec (CALL BY REFERENCE) no change vector
//  // const auto val : vec (COPY BY VALUE) no change local copie
//
//  std::vector<int> vec(10, 0);
//  std::iota(vec.begin(), vec.end(), 1);
//
//  for (auto &val : vec) {
//    val = val * 2;
//    std::cout << val << " " << &val << std::endl;
//  }
//
//  for (int i = 0; i < 10; i++) {
//    std::cout << vec[i] << " " << &vec[i] << std::endl;
//  }
//
//  return 0;
//}