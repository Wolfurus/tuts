//#include <iostream>
//
//using std::cout;
//using std::endl;
//
//// int* input_array <=> int input_array[]
//int array_maximum(int* input_array, unsigned int length) {
//  int max = input_array[0];
//  for (int i = 1; i < length; i++) {
//    if (input_array[i] > max) {
//      max = input_array[i];
//    }
//  }
//
//  return max;
//}
//
//int main() {
//  unsigned int array_size = 10;
//
//  // Heap allocation
//  int *p = new int[array_size];
//
//  for (int i = 0; i < array_size; i++) {
//    p[i] = i + 1;
//  }
//
//  for (int i = 0; i < array_size; i++) {
//    cout << p[i] << endl;
//  }
//
//  array_maximum(p, array_size);
//
//  // Heap de-allocation
//  delete[] p;
//
//  return 0;
//}