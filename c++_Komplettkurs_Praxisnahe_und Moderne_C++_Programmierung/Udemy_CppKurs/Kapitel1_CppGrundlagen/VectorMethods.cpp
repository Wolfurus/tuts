//#include <iostream>
//#include <vector>
//
//int main() {
//  std::vector<float> my_vector;
//
//  for (int i = 0; i < 20; i++) {
//    my_vector.push_back(i);
//  }
//
//  for (int i = 0; i < my_vector.size(); i++) {
//    std::cout << my_vector[i] << std::endl;
//  }
//
//  std::cout << "size: " << my_vector.size() << std::endl;
//  std::cout << "capacity: " << my_vector.capacity() << std::endl;
//
//  my_vector.resize(5);
//  for (int i = 0; i < my_vector.size(); i++) {
//    std::cout << my_vector[i] << std::endl;
//  }
//  std::cout << "size: " << my_vector.size() << std::endl;
//  std::cout << "capacity: " << my_vector.capacity() << std::endl;
//
//  my_vector.clear();
//  std::cout << "size: " << my_vector.size() << std::endl;
//  std::cout << "capacity: " << my_vector.capacity() << std::endl;
//
//  my_vector.shrink_to_fit();
//  std::cout << "size: " << my_vector.size() << std::endl;
//  std::cout << "capacity: " << my_vector.capacity() << std::endl;
//  return 0;
//}