//#include <iostream>
//
//using std::cout;
//using std::endl;
//
//struct Movie {
//  const int year;
//  const char *title;
//  const char *regisseur;
//};
//
//int main() {
//  Movie *star_wars_7=new Movie{2015, "Star Wars the Force Awakens", "J. J. Abrams"};
//  cout << star_wars_7->year << endl;
//  cout << star_wars_7->title << endl;
//  cout << star_wars_7->regisseur << endl;
//  cout << endl;
//
//  Movie star_wars_8{2017, "Star Wars the last Jedi", "Rian Johnson"};
//  cout << star_wars_8.year << endl;
//  cout << star_wars_8.title << endl;
//  cout << star_wars_8.regisseur << endl;
//  cout << endl;
//
//  Movie star_wars_9{2019, "Star Wars the rise of Skywalker", "J. J. Abrams"};
//  cout << star_wars_9.year << endl;
//  cout << star_wars_9.title << endl;
//  cout << star_wars_9.regisseur << endl;
//  cout << endl;
//
//  return 0;
//}