//#include <iostream>
//
//// 1.) + 2.)
//enum Colors { RED = 0, GREEN = 1, BLUE = 2 };
//
//// 1.) + 2.)
//enum Colors2 { RED = 0, GREEN = 1, BLUE = 2 };
//
//// 3.)
//enum class ColorsClass : unsigned int { RED = 0, GREEN = 1, BLUE = 2 };
//
//// 1.) Two Enums cannot share the same names
//// 2.) No variable can have a name which is alreadz in some enumeration
//// 3.) Enums are not tzpe safe
//int main() {
//  // 1.) + 2.)
//	enum Colors c1 = Colors::RED;
//  enum Colors2 c2 = Colors2::RED;
//
//  // 3.)
//  double val = 42.42;
//  if (val == c1) {
//    std::cout << "Equal" << std::endl;
//  }
//
//  enum class ColorsClass c3 = ColorsClass::RED;
//
//  if (val == c3) {
//    std::cout << "Equal" << std::endl;
//  }
//
//  return 0;
//}