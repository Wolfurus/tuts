//#include <iostream>
//
//using std::cin;
//using std::cout;
//using std::endl;
//
//int main() {
//  // Heap allocation
//  int *p_zahl = new int{4};
//
//  // Heap de-allocation
//  delete p_zahl;
//
//  // The memory address of the heap variable, where the pointer points to
//  cout << p_zahl << endl;
//
//  p_zahl = nullptr;
//
//  // The memory address of the heap variable, where the pointer points to
//  cout << p_zahl << endl;
//
//  if (p_zahl != nullptr) {
//    cout << *p_zahl << endl;
//  }
//  return 0;
//}