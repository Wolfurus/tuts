//#include <iostream>
//#include <vector>
//
//int main() {
//  std::vector<int> my_vector(5, 0);
//  // Stadnard ForwardIterator [begin, end)
//  auto it_begin = my_vector.begin();
//  auto it_end = my_vector.end();
//
//  for (auto it = it_begin; it != it_end; ++it) {
//    std::cout << *it << " " << &*it << &it << std::endl;
//  }
//
//  // Const Standard ForwardIterator [begin, end)
//  std::vector<int>::const_iterator const_it_begin = my_vector.begin();
//  std::vector<int>::const_iterator const_it_end = my_vector.end();
//
//  for (auto it = const_it_begin; it != const_it_end; ++it) {
//    std::cout << *it << " " << &*it << &it << std::endl;
//  }
//
//  // Stadnard BidirectionalIterator (end, begin]
//  auto it_rbegin = my_vector.rbegin();
//  auto it_rend = my_vector.rend();
//
//  for (auto it = it_rbegin; it != it_rend; ++it) {
//    std::cout << *it << " " << &*it << &it << std::endl;
//  }
//
//  std::vector<int>::const_reverse_iterator const_it_begin = my_vector.rbegin();
//  std::vector<int>::const_reverse_iterator const_it_end = my_vector.rend();
//
//  for (auto it = const_it_begin; it != const_it_end; ++it) {
//    std::cout << *it << " " << &*it << &it << std::endl;
//  }
//
//  return 0;
//}
