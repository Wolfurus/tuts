//#include <iostream>
//
//using std::cin;
//using std::cout;
//using std::endl;
//
//int main() {
//  // Heap allocation
//  int *p_zahl = new int{4};
//
//  // The memory address of the heap variable, where the pointer points to
//  cout << p_zahl << endl;
//  // The value of the heap variable, where the pointer points to
//  cout << *p_zahl << endl;
//  // Memorz address of the pointer itself
//  cout << &p_zahl << endl;
//
//  // Heap de-allocation
//  delete p_zahl;
//
//  return 0;
//} 