//#include <iostream>
//
//using std::cin;
//using std::cout;
//using std::endl;
//
//int main() {
//  int number = 1337;
//  cout << "Value of number: " << number << endl;
//  cout << "Memory address of number: " << &number << endl;
//
//  // Pointer muss auf eine Speicheradresse zeigen
//  // pointer: Memorz Address
//  // *pointer: Value of that memorz address
//  int *pointer = &number;
//  cout << "The memory address that the variable 'pointer' points to: "
//       << pointer << endl;
//  cout << "The value that the variable 'pointer' points to: " << *pointer
//       << endl;
//
//  return 0;
//}