//#include <iostream>
//
//using std::cout;
//using std::endl;
//
//unsigned int sum(int n) {
//  if (n > 1) {
//    return n + sum(n - 1);
//  } else {
//    return n;
//  }
//}
//
//unsigned int sum_gauss(unsigned int n) { return (n * (n + 1)) / (2); }
//
//int main() {
//  cout << "sum(5) is " << sum(5) << endl;
//  cout << "sum(10) is " << sum(10) << endl;
//  cout << "sum(20) is " << sum(20) << endl;
//  cout << "sum(40) is " << sum(40) << endl;
//  cout << "sum(80) is " << sum(80) << endl;
//
//
//  cout << "gauss(80) is " << sum_gauss(80) << endl;
//
//  return 0;
//}