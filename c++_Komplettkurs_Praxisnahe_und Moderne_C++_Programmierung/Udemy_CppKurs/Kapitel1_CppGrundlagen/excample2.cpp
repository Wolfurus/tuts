//#include <iostream>
//
//#define PI 3.14159
//
//using std::cin;
//using std::cout;
//using std::endl;
//
//int main() {
//  // User-Input: Radius r eines Kreises einlesen
//  // Aufgabe: Berechne den Umfang und den Flächeninhalt des Kreises.
//  // Gebe die berechneten Werte in der Konsole aus.
//
//  double radius;
//  cout << "Please enter the radius of the circle: ";
//  cin >> radius;
//
//  double perimeter = 2 * PI * radius;
//  double area = PI * radius * radius;
//
//  cout << "Perimeter is " << perimeter << endl;
//  cout << "area is " << area << endl;
//
//  return 0;
//}