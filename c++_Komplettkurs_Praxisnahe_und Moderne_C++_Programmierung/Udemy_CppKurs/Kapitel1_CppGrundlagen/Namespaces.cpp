//#include <iostream>
//
//namespace Cinema {
//struct Movie {
//  const int year;
//  const char *title;
//  const char *regisseur;
//};
//} // namespace Cinema
//
//int main() {
//  Cinema::Movie *star_wars_7 =
//      new Cinema::Movie{2015, "Star Wars the Force Awakens", "J. J. Abrams"};
//  std::cout << star_wars_7->year << std::endl;
//  std::cout << star_wars_7->title << std::endl;
//  std::cout << star_wars_7->regisseur << std::endl;
//  std::cout << std::endl;
//
//  Cinema::Movie star_wars_8{2017, "Star Wars the last Jedi", "Rian Johnson"};
//  std::cout << star_wars_8.year << std::endl;
//  std::cout << star_wars_8.title << std::endl;
//  std::cout << star_wars_8.regisseur << std::endl;
//  std::cout << std::endl;
//
//  Cinema::Movie star_wars_9{2019, "Star Wars the rise of Skywalker",
//                            "J. J. Abrams"};
//  std::cout << star_wars_9.year << std::endl;
//  std::cout << star_wars_9.title << std::endl;
//  std::cout << star_wars_9.regisseur << std::endl;
//  std::cout << std::endl;
//
//  return 0;
//}