//#include <cstdlib>
//#include <iostream>
//#include <string>
//#include <string_view>
//
//unsigned int alloc_count = 0;
//
//void *operator new(std::size_t n) {
//  std::cout << "I am allocation on the heap: " << n << std::endl;
//  alloc_count++;
//  return malloc(n);
//}
//
//void do_something1(const std::string &s) { std::cout << s << std::endl; }
//void do_something2(const std::string_view &s) { std::cout << s << std::endl; }
//
//int main() {
//  std::string s("Ich heisse Dietmar und min Lernender auf Udemy.");
//  std::string sub_s(s.substr(4, 16));
//  do_something1(sub_s);
//  std::cout << alloc_count << std::endl;
//
//  std::string_view view_s(s.c_str() + 4, 16);
//  do_something2(view_s);
//  std::cout << alloc_count << std::endl;
//  return 0;
//}