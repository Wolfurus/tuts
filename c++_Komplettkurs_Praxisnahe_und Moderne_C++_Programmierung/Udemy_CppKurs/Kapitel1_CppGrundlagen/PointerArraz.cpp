//#include <iostream>
//
//using std::cout;
//using std::endl;
//
//int main() {
//  unsigned int array_size = 10;
//
//  // Heap allocation
//  int *p = new int[array_size];
//
//  for (int i = 0; i < array_size; i++) {
//    p[i] = i+1;
//  }
//
//  for (int i = 0; i < array_size; i++) {
//    cout << p[i] << endl;
//  }
//
//  // Heap de-allocation
//  delete[] p;
//
//  return 0;
//}