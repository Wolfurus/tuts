//#include <iostream>
//#include <string>
//#include <tuple>
//#include <utility>
//#include <vector>
//
//typedef std::tuple<int, std::string, bool> tuple_user;
//typedef std::pair<int, std::string> pair_user;
//
//void print_tuple(tuple_user &tpl) {
//  std::cout << std::get<0>(tpl) << std::endl;
//  std::cout << std::get<1>(tpl) << std::endl;
//  std::cout << std::get<2>(tpl) << std::endl;
//  std::cout << std::endl;
//}
//
//void print_pair(pair_user &pair) {
//  std::cout << pair.first << std::endl;
//  std::cout << pair.second << std::endl;
//  std::cout << std::endl;
//}
//
//void print_students(std::vector<tuple_user>& students) {
//  for (auto&stud:students) {
//    print_tuple(stud);
//  }
//}
//
//int main() {
//  // std::pair hat immer zwei Werte
//  // std::tuple hat n Werte
//
//  tuple_user my_tpl1(10, "Mark", true);
//  tuple_user my_tpl2 =
//      std::make_tuple(42, "Dietmar", false);
//
//  print_tuple(my_tpl1);
//  print_tuple(my_tpl2);
//
//  std::vector<tuple_user> students;
//  students.push_back(my_tpl1);
//  students.push_back(my_tpl2);
//
//  std::pair<int, std::string> my_pair1(11,"World");
//  std::pair<int, std::string> my_pair2 = std::make_pair(10,"Hallo");
//  print_pair(my_pair1);
//  print_pair(my_pair2);
//
//  return 0;
//}
