//#include <iostream>
//#include <math.h>
//#include <vector>
//
//void fill_double_vector(std::vector<double> &vector) {
//  for (size_t i = 0; i < vector.size(); i++) {
//    vector[i] = i;
//  }
//}
//
//void print_double_vector(std::vector<double> &vector) {
//  for (size_t i = 0; i < vector.size(); i++) {
//    if (i > 0) {
//      std::cout << "; ";
//    }
//    std::cout << vector[i];
//  }
//  std::cout << std::endl;
//}
//
//void exp_double_vector(std::vector<double> &vector) {
//  for (auto it = vector.begin(); it != vector.end(); ++it) {
//    *it = exp(*it);
//  }
//}
//
//void log_double_vector(std::vector<double> &vector) {
//  for (auto vec : vector) {
//    vec = log(vec);
//  }
//}
//
//int main() {
//  std::vector<double> my_vector(9, 0);
//  fill_double_vector(my_vector);
//  print_double_vector(my_vector);
//
//  exp_double_vector(my_vector);
//  print_double_vector(my_vector);
//
//  log_double_vector(my_vector);
//  print_double_vector(my_vector);
//  return 0;
//}