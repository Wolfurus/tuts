//#include <forward_list>
//#include <iostream>
//#include <vector>
//
//int main() {
//  std::vector<int> my_vector = {1, 2};
//
//  for (int i = 0; i < my_vector.size(); i++) {
//    std::cout << &my_vector[i] << std::endl;
//  }
//
//  std::cout << std::endl;
//
//  std::forward_list<int> my_flist;
//  my_flist.assign({1, 2,4,5});
//
//  for (auto it = my_flist.begin(); it != my_flist.end(); it++) {
//    std::cout << *it << " - " << &*it << std::endl;
//  }
//  std::cout << std::endl;
//
//  my_flist.pop_front();
//  for (auto it = my_flist.begin(); it != my_flist.end(); it++) {
//    std::cout << *it << " - " << &*it << std::endl;
//  }
//  std::cout << std::endl;
//  my_flist.remove(3);
//  for (auto it = my_flist.begin(); it != my_flist.end(); it++) {
//    std::cout << *it << " - " << &*it << std::endl;
//  }
//  std::cout << std::endl;
//  my_flist.push_front(5);
//  for (auto it = my_flist.begin(); it != my_flist.end(); it++) {
//    std::cout << *it << " - " << &*it << std::endl;
//  }
//  std::cout << std::endl;
//  my_flist.insert_after(my_flist.begin()++, 6);
//  for (auto it = my_flist.begin(); it != my_flist.end(); it++) {
//    std::cout<< *it <<" - " << &*it << std::endl;
//  }
//}
