//#include <algorithm>
//#include <iostream>
//#include <set>
//#include <string>
//
//void print_set(std::set<std::string> &set) {
//  for (const auto &val : set) {
//    std::cout << val << std::endl;
//  }
//  std::cout << std::endl;
//}
//
//int main() {
//  std::set<std::string> my_set1;
//  std::set<std::string> my_set2;
//
//  my_set1.insert("Jan");
//  my_set1.insert("Dietmar");
//  my_set1.insert("Peter");
//  print_set(my_set1);
//
//  my_set2.insert("Nina");
//  my_set2.insert("Dietmar");
//  my_set2.insert("Mats");
//  print_set(my_set2);
//
//  // Set Union
//  std::set<std::string> my_set3;
//  std::set_union(my_set1.begin(), my_set1.end(), my_set2.begin(), my_set2.end(),
//                 std::inserter(my_set3, my_set3.end()));
//  print_set(my_set3);
//
//  std::set<std::string> my_set4;
//  std::set_intersection(my_set1.begin(), my_set1.end(), my_set2.begin(),
//                        my_set2.end(), std::inserter(my_set3, my_set3.end()));
//  print_set(my_set4);
//
//  std::set<std::string> my_set5;
//  std::set_difference(my_set1.begin(), my_set1.end(), my_set2.begin(),
//                      my_set2.end(), std::inserter(my_set3, my_set3.end()));
//  print_set(my_set5);
//
//  std::set<std::string> my_set6;
//  std::set_symmetric_difference(my_set1.begin(), my_set1.end(), my_set2.begin(),
//                                my_set2.end(),
//                                std::inserter(my_set3, my_set3.end()));
//  print_set(my_set6);
//  
//  return 0;
//}
