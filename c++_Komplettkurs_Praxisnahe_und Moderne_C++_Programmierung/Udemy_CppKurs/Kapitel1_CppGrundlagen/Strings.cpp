//#include <fstream>
//#include <iostream>
//#include <string>
//
//std::string read_text(const std::string &path) {
//  std::string str;
//  std::string text;
//  std::ifstream iffile;
//  iffile.open(path);
//  if (iffile.is_open()) {
//    while (std::getline(iffile, str)) {
//      text += str + "\n";
//    }
//  }
//  iffile.close();
//  return text;
//}
//
//void write_text(const std::string &path, const std::string &text) {
//  std::ofstream offile;
//  offile.open(path);
//  offile << text;
//  offile.close();
//}
//
//int main() {
//  std::string text = read_text("text.txt");
//  std::cout << text << std::endl;
//
//  // Find und NPOS
//  // first occurence
//  std::string search = "vier";
//  std::size_t first_idx = text.find(search);
//  if (first_idx != std::string::npos) {
//    std::cout << search << " wurde gefunden!" << std::endl;
//  } else {
//    std::cout << "Leider nicht gefunden!" << std::endl;
//  }
//
//  // rfind
//  // last occurence
//  std::string search2 = "drei";
//  std::size_t first_idx2 = text.rfind(search2);
//  if (first_idx2 != std::string::npos) {
//    std::cout << search2 << " wurde gefunden!" << std::endl;
//  } else {
//    std::cout << "Leider nicht gefunden!" << std::endl;
//  }
//
//  // Substrings
//  std::string substr_search = text.substr(first_idx, search.length());
//  std::cout << "Substr: " << substr_search << std::endl;
//
//  // Replace
//  std::string new_substr = "three";
//  text.replace(first_idx, first_idx + search.length(), new_substr);
//  std::cout << text << std::endl;
//
//  // Compare
//  std::string s1 = "Hallo";
//  std::string s2 = "Hello";
//
//  std::cout << "String compare: " << s1.compare(s2) << std::endl;
//
//  return 0;
//}