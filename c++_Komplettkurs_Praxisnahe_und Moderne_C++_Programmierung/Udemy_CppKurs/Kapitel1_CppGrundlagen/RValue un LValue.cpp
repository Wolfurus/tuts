//#include <iostream>
//
//// Const LValue Reference
//void f(const int &z) { std::cout << z << " " << &z << std::endl << std::endl; }
//
//// RValue
//void f(int &&z) { std::cout << z << " " << &z << std::endl << std::endl; }
//
//// LValue Reference
//void f(int &z) { std::cout << z << " " << &z << std::endl << std::endl; }
//
//// LValue (Copz by value)
//void f(int z) { std::cout << z << " " << &z << std::endl << std::endl; }
//
//// LValue (Left): Variable mit einer bestimmten Speicher-Adresse
//// RValue (Right): "Variable" ohne einer bestimmten Speicher-Adresse
//int main() {
//  int a = 3;       // LValue
//  const int b = 3; // const LValue
//  int &c = a;      // LValue reference
//
//  std::cout << a << " " << &a << std::endl;
//  f(a); // LValue
//
//  std::cout << b << " " << &b << std::endl;
//  f(b); // const LValue
//
//  std::cout << c << " " << &c << std::endl;
//  f(c); // LValue reference
//
//  f(3); // RValue
//
//  return 0;
//}